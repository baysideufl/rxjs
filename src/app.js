import * as rxjs from "rxjs"
import * as op from "rxjs/operators";
import { ajax } from "rxjs/ajax";

import "materialize-css/dist/js/materialize.min.js";

import "./app.scss";


// --------------------------------------
// From Event - Mouse Move
let mouseMove$ = rxjs.fromEvent(document.getElementById('mousemove-card'), 'mousemove');
let mouseMoveOutput = document.getElementById('mousemove-output');

mouseMove$
  .pipe(
    op.throttleTime(200),
    // op.debounceTime(200),
    // op.filter(e =>
    //    e.layerX > 0
    //    && e.layerX < 200
    //    && e.layerY > 0
    //    && e.layerY < 200
    // ),
    op.tap(x => console.log(x)),
    op.map(e => {
      return {
        clientX: e.clientX,
        clientY: e.clientY,
        layerX: e.layerX,
        layerY: e.layerY,
        altKey: e.altKey,
        ctrlKey: e.ctrlKey,
        metaKey: e.metaKey,
        shiftKey: e.shiftKey
      }
    })
  )
  .subscribe(x => {
    mouseMoveOutput.innerText = JSON.stringify(x, null, 2);
  });



// --------------------------------------
// Ajax - CHUCK NORRIS
let chuck$ = rxjs.fromEvent(document.getElementById("chuck-button"), 'click');
let chuckOutput = document.getElementById('chuck-output');

chuck$
  .pipe(
    // switchMap() only keeps last request. previous requests are cancelled.
    op.switchMap(() => {
      // getJSON() uses getXMLHttpRequest under the hood
      return ajax.getJSON("https://api.chucknorris.io/jokes/random")
    })
  )
  .subscribe(x => {
    chuckOutput.innerText = x.value;
  });



// --------------------------------------
// Double Click
let doubleClickButton$ = rxjs.fromEvent(document.getElementById("doubleclick-button"), 'click');
let doubleclickOutput = document.getElementById('doubleclick-output');

doubleClickButton$
  .subscribe(() => console.log('click'));

let doubleClicks$ = doubleClickButton$
  .pipe(
    op.buffer(
      doubleClickButton$.pipe(op.debounceTime(300))
    ),
    op.map(x => x.length),
    op.tap(x => console.log('map:', x)),
    op.filter(x => x >= 2)
  );

// print the double clicks
doubleClicks$
  .subscribe(x => {
    doubleclickOutput.innerHTML = `<h5>Double+ Click: ${x}</h5>`;
  });

// reset the output afte 1s
doubleClicks$
  .pipe(op.debounceTime(1000))
  .subscribe(x => {
    doubleclickOutput.innerHTML = "<h5>Fart in the wind...</h5>";
  });



// --------------------------------------
// Merge - Timers

let mergeoutput = document.getElementById('merge-output');
rxjs.fromEvent(document.getElementById("merge-button"), 'click')
  .subscribe(() => {
    mergeoutput.innerHTML = "<h5>Go baby go!</h5>";
    initMerge();
  });


function initMerge() {
  //emit every 2.5 seconds
  const first$ =  rxjs.interval(1000);
  //emit every 2 seconds
  const second$ = rxjs.interval(2000);
  //emit every 1.5 seconds
  const third$ = rxjs.interval(3000);
  //emit every 1 second
  const fourth$ = rxjs.interval(4000);

  //emit outputs from one observable
  const example$ = rxjs.merge(
    first$.pipe(op.mapTo('FIRST!'), op.take(3)),
    second$.pipe(op.mapTo('SECOND!'), op.take(3)),
    third$.pipe(op.mapTo('THIRD'), op.take(3)),
    fourth$.pipe(op.mapTo('FOURTH'), op.take(3))
  );
  //output: "FOURTH", "THIRD", "SECOND!", "FOURTH", "FIRST!", "THIRD", "FOURTH"
  const subscribe = example$.subscribe(val => mergeoutput.innerHTML += `<p>${val}</p>`);
}



// --------------------------------------
// CombineLatest - Timers

let combinelatest = document.getElementById('combinelatest-output');
rxjs.fromEvent(document.getElementById("combinelatest-button"), 'click')
  .subscribe(() => {
    combinelatest.innerHTML = "<h5>Go baby go!</h5>";
    initCombineLatest();
  });

function initCombineLatest() {

  //timerOne emits first value at 1s, then once every 4s
  const timerOne$ = rxjs.timer(1000, 4000).pipe(op.take(3));
  //timerTwo emits first value at 2s, then once every 4s
  const timerTwo$ = rxjs.timer(2000, 4000).pipe(op.take(3));
  //timerThree emits first value at 3s, then once every 4s
  const timerThree$ = rxjs.timer(3000, 4000).pipe(op.take(3));


  //when one timer emits, emit the latest values from each timer as an array
  const combined$ = rxjs.combineLatest(timerOne$, timerTwo$, timerThree$);

  combined$.subscribe(
    ([timerValOne, timerValTwo, timerValThree]) => {
      combinelatest.innerHTML += `<p>Timer One Latest: ${timerValOne}</p>
      <p>Timer Two Latest: ${timerValTwo}</p>
      <p>Timer Three Latest: ${timerValThree}</p><p><hr></p>`;

    }
  );
}

// --------------------------------------
// --------------------------------------


