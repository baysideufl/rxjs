# Just some RxJS stuff

```bash
$> yarn start
```

## References

  - [RxJS @ Firebase App](https://rxjs-dev.firebaseapp.com)
  - [What's new in RxJS 6](https://auth0.com/blog/whats-new-in-rxjs-6/)
  - [RxJS Observables](https://x-team.com/blog/rxjs-observables/)
  - [ReactiveX RxJS Github](https://github.com/ReactiveX/rxjs)
  - [Learn RxJS](https://www.learnrxjs.io/)
  - [RxJS Observables Tutorial](https://coursetro.com/posts/code/148/RxJS-Observables-Tutorial---Creating-&-Subscribing-to-Observables)
  - [Practical RxJS](https://blog.angularindepth.com/practical-rxjs-in-the-wild-requests-with-concatmap-vs-mergemap-vs-forkjoin-11e5b2efe293)